import math
import numpy as np
import pickle

from typing import Any, List

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint

import logging

logger = logging.getLogger(__name__)


@register_filter("svm_pose_estimator")
class SVMPoseEstimator(Filter):
    """
    Pose estimation filter using SVM. This filter currently works wih pkl files generated using the framework
    described in https://gitlab.com/sat-mtl/tools/livepose/research-and-development/frame_based_pose_recognition. 

    The SVM filter only works with the mediapipe pose backend for now. Outputs the name of the recognized gesture.

    To test the filter, you can download the following model and assing it to the model_path in the config file: 
    https://gitlab.com/sat-mtl/tools/livepose/research-and-development/frame_based_pose_recognition/-/blob/main/model.pkl 
    """

    def __init__(self, model_path: str, *args: Any, **kwargs: Any):
        super(SVMPoseEstimator, self).__init__("svm_pose_estimator")

        self._model_path = model_path

        try:
            self._svm_model = pickle.load(open(self._model_path, 'rb'))
        except Exception as e:
            logger.warning(f"Unable to load model at path {self._model_path}: {str(e)}")
            self._svm_model = None

        

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        super().step(flow=flow, now=now, dt=dt)

        if self._svm_model is None:
            return

        result: Filter.Result = {}

        # Get all pose streams
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        # All 2D poses from all streams are sent
        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                for cam_id, poses_for_camera in enumerate(poses_for_cameras):
                    result[cam_id] = {}

                    for pose_index, pose in enumerate(poses_for_camera.poses):
                        ordered_keypoints: List[float] = []
                        keypoint_dict = pose.keypoints
                        if not keypoint_dict:
                            ordered_keypoints = np.zeros([1,66], dtype=float)
                            continue

                        # Fill in the values for not detected joints
                        
                        for name, keypoint in pose.keypoints.items():
                            ordered_keypoints.append(keypoint.position[0])
                            ordered_keypoints.append(keypoint.position[1])
                        
                        reordered_keypoints : np.array = np.array(ordered_keypoints)

                        # # Compute the distance between all pair of joints
                        # joints_features: List[float] = []
                        # for i in ordered_keypoints:
                        #     for j in ordered_keypoints:
                        #         dist_i_j = math.sqrt((i[0] - j[0])**2 + (i[1] - j[1])**2)
                        #         joints_features.append(dist_i_j)

                        # Predict the gesture
                        gesture = self._svm_model.predict(reordered_keypoints.reshape(-1,66))[0]
                    
                        logger.info(f"Currently detected gesture: {gesture}")

                        result[cam_id][pose_index] = gesture

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
