import numpy as np

from typing import Any, Dict, List

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


@register_filter("number_id")
class NumberIDFilter(Filter):
    """
    Filter which outputs the number of skeletons detected by camera.

    The output is as follows:
    {"numberID/{camera_id}": [Number of detected skeletons]}
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(NumberIDFilter, self).__init__("number_id", **kwargs)

    def add_parameters(self) -> None:
        pass

    def init(self) -> None:
        pass

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        # Get all pose streams
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        # All 2D poses from all streams are sent
        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                for cam_id, poses_for_camera in enumerate(poses_for_cameras):
                    nb_of_ids = len(poses_for_camera.poses)
                    result[cam_id] = nb_of_ids

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
