import numpy as np

from typing import Any, Dict, List

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


@register_filter("pointing")
class Pointing(Filter):
    """
    A skeleton would be considered "pointing" if either of the wrist keypoints are higher than the MID_HIP.

    For more information about the bones:
    https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#keypoint-ordering

    OSC message is as follows, for frame:
    {camera number}/{detected person}/{left}/{pointing} {True or False}
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(Pointing, self).__init__("pointing", **kwargs)

    def add_parameters(self) -> None:
        pass

    def init(self) -> None:
        pass

    def get_y(self, keypoint: Any, threshold: float):
        """
        get_y the filter
        :param keypoint: List[np.ndarray] or np.ndarray - one of the wrists aor hip keypoints
        :param threshold: float - confidence of keypoint detection

        Returns:
        keypoint_y: List[np.float] or np.float: y coordinate of the input keypoints
        """

        keypoint_y = []

        if isinstance(keypoint, list):
            for k in keypoint:
                if k is not None and k.confidence > threshold:
                    keypoint_y.append(k.position[1])

        elif isinstance(keypoint, Keypoint):
            if keypoint is not None and keypoint.confidence > threshold:
                return keypoint.position[1]

        return keypoint_y

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Update the filter
        :param flow: Flow - Data flow to read from and write to
        :param now: Current time
        :param dt: Time since last call
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)
        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                threshold = 0.5
                for cam_id, poses_for_camera in enumerate(poses_for_cameras):
                    result[cam_id] = {}
                    for pose_index, pose in enumerate(poses_for_camera.poses):
                        result[cam_id][pose_index] = {}

                        keypoint_dict = pose.keypoints
                        if not keypoint_dict:
                            continue

                        left_pointing = False
                        right_pointing = False

                        try:
                            left_wrist_keypoint = np.array(keypoint_dict["LEFT_WRIST"])
                        except KeyError:
                            print("LEFT_WRIST keypoints not detected")

                        try:
                            right_wrist_keypoint = np.array(keypoint_dict["RIGHT_WRIST"])
                        except KeyError:
                            print("RIGHT_WRIST keypoints not detected")

                        try:
                            mid_hip_keypoints: List[np.array] = [
                                np.array(keypoint_dict["RIGHT_HIP"]),
                                np.array(keypoint_dict["LEFT_HIP"])
                            ]
                        except KeyError:
                            print("HIP keypoints not detected")

                        left_wrist_keypoint_y = self.get_y(left_wrist_keypoint, threshold)
                        right_wrist_keypoint_y = self.get_y(right_wrist_keypoint, threshold)
                        mid_hip_keypoints_y = self.get_y(mid_hip_keypoints, threshold)

                        # Check if each elbow or wrist is higher than nose or neck.
                        if left_wrist_keypoint is not None:
                            if any(left_wrist_keypoint_y < mid_hip_keypoints_y):
                                left_pointing = True
                        else:
                            pass

                        if right_wrist_keypoint is not None:
                            if any(right_wrist_keypoint_y < mid_hip_keypoints_y):
                                right_pointing = True
                        else:
                            pass

                        result[cam_id][pose_index]["left"] = left_pointing
                        result[cam_id][pose_index]["right"] = right_pointing

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
