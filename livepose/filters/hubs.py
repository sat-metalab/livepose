import json
import numpy as np
import warnings

from typing import Any, Dict, List, Optional, Tuple

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


@register_filter("hubs")
class HubsFilter(Filter):
    """
    Filter which controls Hubs avatar through WebSocket
    All coordinates are normalized between -1.0 and 1.0 in the image frame
    (-1.0, -1.0) is in the lower left corner of the image frame
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(HubsFilter, self).__init__("hubs", **kwargs)

    def add_parameters(self) -> None:
        pass

    def init(self) -> None:

        self.bones_positions: Dict[str, Optional[Tuple[float, float]]] = {}
        self.bones_id: Dict[str, int] = {
            "HEAD": 0,
            "LEFT_SHOULDER": 5,
            "RIGHT_SHOULDER": 2,
            "LEFT_ELBOW": 6,
            "RIGHT_ELBOW": 3,
            "LEFT_HAND": 7,
            "RIGHT_HAND": 4
        }

        # We use the mean of the nb_samples last samples
        self.nb_samples = 8
        self.indice = 0  # indice of the sample to be written into the array
        self.bones_samples: Dict[str, np.array] = {}
        array_template = np.empty([2, self.nb_samples])
        for bone_name in self.bones_id.keys():
            self.bones_samples[bone_name] = np.full_like(array_template, np.nan)

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        # Get all pose streams
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        # Get the shape of the first available input frame
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)
        assert(len(input_streams) > 0)
        color_channels = [*input_streams.values()][0].get_channels_by_type(Channel.Type.COLOR)
        assert(len(color_channels) > 0)
        frame_shape = color_channels[0].data.shape

        # We take the first pose detected by the backend
        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                cam_id = 0
                pose_index = 0
                if (not poses_for_cameras or not poses_for_cameras[cam_id] or not poses_for_cameras[cam_id].poses[pose_index]):
                    return

                keypoint_dict = poses_for_cameras[cam_id].poses[pose_index].keypoints
                keypoint_defs = poses_for_cameras[cam_id].poses[pose_index].keypoints_definitions

                # Detection confidence threshold.
                threshold = 0.2

                for bone, index in self.bones_id.items():
                    keypoint = keypoint_dict.get(list(keypoint_defs)[index])  # type: ignore
                    bone_samples = self.bones_samples[bone]
                    x_samples = bone_samples[0]
                    y_samples = bone_samples[1]
                    if keypoint is not None and keypoint.confidence > threshold:
                        x_samples[self.indice] = 1 - 2 * float(keypoint.position[0]) / frame_shape[1]
                        y_samples[self.indice] = 1 - 2 * float(keypoint.position[1]) / frame_shape[0]
                    else:
                        x_samples[self.indice] = np.nan
                        y_samples[self.indice] = np.nan

                    # Setting the result to the mean of last samples
                    with warnings.catch_warnings():
                        # silencing annoying "RuntimeWarning: Mean of empty slice"
                        warnings.simplefilter("ignore", category=RuntimeWarning)
                        x = np.nanmean(x_samples)
                        y = np.nanmean(y_samples)

                    result[bone] = ([x, y] if not np.isnan(x) and not np.isnan(y) else None)

                self.indice = (self.indice + 1) % self.nb_samples

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
