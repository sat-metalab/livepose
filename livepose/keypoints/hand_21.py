from typing import Any, Dict

"""21 hand keypoints, as outlined in the trt_pose_hand repo compatible with OpenPose ordering.
Since there are a lot of these keypoints and they are not as easily
nameable as the body keypoints, we simply name them by their index for
clarity:
PALM, THUMB_1, etc.
"""
Hand21Keypoints: Dict[str, Any] = {
    "PALM": 0,
    "THUMB_1": 1,
    "THUMB_2": 2,
    "THUMB_3": 3,
    "THUMB_4": 4,
    "INDEX_FINGER_1": 5,
    "INDEX_FINGER_2": 6,
    "INDEX_FINGER_3": 7,
    "INDEX_FINGER_4": 8,
    "MIDDLE_FINGER_1": 9,
    "MIDDLE_FINGER_2": 10,
    "MIDDLE_FINGER_3": 11,
    "MIDDLE_FINGER_4": 12,
    "RING_FINGER_1": 13,
    "RING_FINGER_2": 14,
    "RING_FINGER_3": 15,
    "RING_FINGER_4": 16,
    "BABY_FINGER_1": 17,
    "BABY_FINGER_2": 18,
    "BABY_FINGER_3": 19,
    "BABY_FINGER_4": 20,
}
