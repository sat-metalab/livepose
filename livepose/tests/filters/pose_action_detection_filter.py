"""Unit tests for livepose/filters/armup.py"""

from unittest import TestCase

from livepose.dataflow import Flow
from livepose.filters.pose_action_detection import PoseActionDetection


class TestStep(TestCase):
    def setUp(self):
        self.filter = PoseActionDetection()
        self.filter.init()

    def test_no_keypoints(self):
        """Test if no keypoints are provided, filter.result is empty"""
        self.assertEqual(self.filter.result, {})

        self.filter.step(
            flow=Flow(),
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[self.filter._filter_name], {})
