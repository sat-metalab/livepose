import PIL.Image
import json
import logging
import os
import sys
from time import time

import cv2
import math
import numpy as np

from dataclasses import dataclass
from typing import Any, Dict, List

from livepose.pose_backend import PosesForCamera, Pose, Keypoint, PoseBackend, register_pose_backend
from livepose.dataflow import Channel, Flow, Stream
from livepose.node import ranged_type

from livepose.keypoints.face_70 import Face70Keypoints
from livepose.keypoints.hand_21 import Hand21Keypoints
from livepose.keypoints.body_25 import Body25Keypoints

logger = logging.getLogger(__name__)


MODEL_DIR = os.path.realpath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "../models/trt"
))


try:
    import torch
    has_torch = True
except ImportError:
    has_torch = False

try:
    import torch2trt
    has_torch2trt = True
except ImportError:
    has_torch2trt = False

try:
    import torchvision.transforms as transforms
    has_torchvision = True
except ImportError:
    has_torchvision = False

try:
    import pycocotools
    has_pycocotools = True
except ImportError:
    has_pycocotools = False

try:
    import trt_pose.coco
    import trt_pose.models
    from trt_pose.draw_objects import DrawObjects
    from trt_pose.parse_objects import ParseObjects
    has_trt_pose = True
except ImportError:
    has_trt_pose = False


@register_pose_backend("trt")
class TensorRTBackend(PoseBackend):

    @dataclass
    class DetectionResult:
        image: np.array
        counts: int
        objects: Any
        peaks: Any

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()

        # Option to optimize model or load pre-optimized model
        self._parser.add_argument('--optimize', type=bool, default=False,
                                  help="Whether to force re-optimizing the model or not.")

        self._parser.add_argument('--keypoint-dataset', type=str, default="BODY_25", choices=["BODY_25", "FACE_70", "HAND_21"],
                                  help="Identifier of keypoint dataset.")

        self._parser.add_argument('--model-path', type=str, default=MODEL_DIR,
                                  help="Path where model weights and description files are stored.")

        self._parser.add_argument('--optimized-model', type=str, default='resnet18_baseline_att_224x224_A_epoch_249_trt.pth',
                                  help='Optimized model weights filename. This file will be automatically created by LivePose and re-generated when required by dependencies updates. We recommend to name it similarly to `"model_weights"`, with `_trt.pth` suffix.')

        self._parser.add_argument('--model-desc', type=str, default='human_pose.json',
                                  help="JSON file which describes the human pose task in [COCO](https://cocodataset.org) format, it is the category descriptor pulled from the annotations file.")

        self._parser.add_argument('--model-weights', type=str,
                                  default='resnet18_baseline_att_224x224_A_epoch_249.pth', help="Model weights filename.")

        self._parser.add_argument('--model-width', type=int, default=224,
                                  help="Input width that the model was trained with.")

        self._parser.add_argument('--model-height', type=int, default=224,
                                  help="Input height that the model was trained with.")
        # Confidence threshold for a single detected person. Any detected
        # person instance with a whole confidence score below this threshold
        # will have NONE of their keypoints displayed. This overrides any
        # keypoints above the self._min_keypoint_confidence threshold.
        self._parser.add_argument('--min-pose-confidence', type=ranged_type(float, 0.0, 1.0), default=0.15,
                                  help="Confidence threshold for a single detected person.")

    def init(self) -> None:

        self._keypoint_dict: Dict[str, int]
        if self._args.keypoint_dataset == "BODY_25":
            self._keypoint_dict = Body25Keypoints
        elif self._args.keypoint_dataset == "FACE_70":
            self._keypoint_dict = Face70Keypoints
        elif self._args.keypoint_dataset == "HAND_21":
            self._keypoint_dict = Hand21Keypoints
        else:
            logger.info(f"trt: unsupported keypoint dataset {self._args.keypoint_dataset} ...")
            exit(1)

        if not has_torch or not has_torchvision or not has_torch2trt or not has_trt_pose or not has_pycocotools:
            if not has_torch:
                logger.error("torch library could not be found. Have you installed it?")
            if not has_torchvision:
                logger.error("torchvision library could not be found. Have you installed it?")
            if not has_torch2trt:
                logger.error("torch2trt library could not be found. Have you installed it?")
            if not has_trt_pose:
                logger.error("trt_pose library could not be found. Have you installed it?")
            if not has_pycocotools:
                logger.error("pycocotools library could not be found. Have you installed it?")
            sys.exit(1)

        trt_model_path = os.path.join(os.environ['HOME'], ".local", "livepose", "models", "trt")
        os.makedirs(trt_model_path, exist_ok=True)
        optimized_model_path = os.path.realpath(os.path.join(trt_model_path, self._args.optimized_model))

        # Test if CUDA acceleration is active, otherwise load_state_dict may produce a segmentation fault that we can not catch
        # TODO change architecture to test is_gpu_acceleration_active before loading models from backends
        cv2_cuda_enabled_device_count = 0
        try:
            cv2_cuda_enabled_device_count = cv2.cuda.getCudaEnabledDeviceCount()
        except cv2.error as e:
            logger.error(f"{e.msg}")
            if e.code == -217:
                # -217: Gpu API call
                if e.err.find("forward compatibility was attempted on non supported HW") != -1 or e.err.find("system has unsupported display driver / cuda driver combination") != -1:
                    logger.error(
                        f"This error may be due to a recent upgrade of Nvidia drivers, if so you may need to reboot your computer.")
                sys.exit(1)

        # Try to load the optimized model using *torch2trt* as follows.
        if not self._args.optimize:
            self._model_trt = torch2trt.TRTModule()
            logger.info(f"trt: loading optimized model {optimized_model_path} ...")
            try:
                self._model_trt.load_state_dict(torch.load(optimized_model_path))
            except AttributeError as err:
                logger.warn(
                    f"trt: could not load optimized model {optimized_model_path} due to error {err.args} that might be following error \"The engine plan file is not compatible with this version of TensorRT\", re-optimizing ...")
                self._args.optimize = True
            except Exception as e:
                logger.warn(
                    f"trt: could not load optimized model {optimized_model_path} due to error {e}, re-optimizing ...")
                self._args.optimize = True

        # First, let's load the JSON file which describes the human pose task.
        # This is in COCO format, it is the category descriptor pulled from the annotations file.
        # We modify the COCO category slightly, to add a neck keypoint.
        # We will use this task description JSON to create a topology tensor, which is an intermediate data structure that describes the part linkages, as well as which channels in the part affinity field each linkage corresponds to.
        model_desc_path = os.path.realpath(os.path.join(self._args.model_path, self._args.model_desc))
        with open(model_desc_path, 'r') as f:
            human_pose = json.load(f)

        self._topology = trt_pose.coco.coco_category_to_topology(human_pose)

        self._parts = trt_pose.coco.coco_category_to_parts(human_pose)

        if self._args.optimize:
            # Next, we'll load our model.
            # Each model takes at least two parameters,
            # *cmap_channels* corresponding to the number of heatmap channels
            # and *paf_channels* to the part affinity field channels.
            # The number of part affinity field channels is 2x the number of links, because each link has a channel corresponding to the x and y direction of the vector field for each link.

            num_parts = len(human_pose['keypoints'])
            num_links = len(human_pose['skeleton'])

            model = trt_pose.models.resnet18_baseline_att(num_parts, 2 * num_links).cuda().eval()

            # Next, let's load the model weights.
            model_weights_path = os.path.realpath(os.path.join(self._args.model_path, self._args.model_weights))
            logger.info(f"trt: loading model {model_weights_path} ...")
            model.load_state_dict(torch.load(model_weights_path))

            # In order to optimize with TensorRT using the python library *torch2trt* we'll also need to create some example data. #
            # The dimensions of this data should match the dimensions that the network was trained with.
            # Since we're using the resnet18 variant that was trained on an input resolution of 224x224, we set the width and height to these dimensions.

        if self._args.optimize:
            data = torch.zeros((1, 3, self._args.model_height, self._args.model_width)).cuda()

            # Next, we'll use [torch2trt](https://github.com/NVIDIA-AI-IOT/torch2trt) to optimize the model.
            # We'll enable fp16_mode to allow optimizations to use reduced half precision.
            logger.info(f"trt: optimizing model {model_weights_path} ... (this may take a while)")
            start_time = time()
            self._model_trt = torch2trt.torch2trt(model, [data], fp16_mode=True, max_workspace_size=1 << 25)
            now = round(time() - start_time)
            logger.info(f"trt: optimizing model {model_weights_path} took {now} seconds")

        # # The optimized model may be saved so that we do not need to perform optimization again, we can just load the model.
        # # Please note that TensorRT has device specific optimizations, so you can only use an optimized model on similar platforms.
        if self._args.optimize:
            logger.info(f"trt: saving optimized model {optimized_model_path} ...")
            torch.save(self._model_trt.state_dict(), optimized_model_path)

        self._mean = torch.Tensor([0.485, 0.456, 0.406]).cuda()
        self._std = torch.Tensor([0.229, 0.224, 0.225]).cuda()
        self._device = torch.device('cuda')

        # Next, we'll define two callable classes that will be used to parse the objects from the neural network, as well as draw the parsed objects on an image.
        self._parse_objects = ParseObjects(self._topology)
        self._draw_objects = DrawObjects(self._topology)

        # This attribute will hold the detection result
        self._detection_results: List["TensorRTBackend.DetectionResult"] = []

    # Next, let's define a function that will preprocess the image, which is originally in BGR8 / HWC format.
    def preprocess(self, image) -> torch.Tensor:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = PIL.Image.fromarray(image)
        image = transforms.functional.to_tensor(image).to(self._device)
        image.sub_(self._mean[:, None, None]).div_(self._std[:, None, None])
        return image[None, ...]

    def draw_objects(self) -> None:
        """
        Draw detected objects onto the source image
        """
        tracked_images = []

        for detection_result in self._detection_results:
            self._draw_objects(
                detection_result.image,
                detection_result.counts,
                detection_result.objects,
                detection_result.peaks
            )

            tracked_images.append(detection_result.image)

        self._tracked_images = tracked_images

    def stop(self) -> None:
        """Stop DL Backend model."""
        pass

    def start(self) -> bool:
        """Start the DL Backend model.
        :return: bool - success of start. Returns False if there is no valid
        image generator, model path, etc.
        """
        if self._args.model_path is None:
            return False

        return True

    def step_pose_detection(self, flow: Flow, now: float, dt: float) -> bool:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)
        detection_results: List[TensorRTBackend.DetectionResult] = []

        if len(input_streams) == 0:
            return False

        keypoints_by_cam: List[PosesForCamera] = []

        for stream in input_streams.values():
            if Channel.Type.COLOR not in stream.channel_types:
                continue

            image: np.array = stream.get_channels_by_type(Channel.Type.COLOR)[0].data

            h = image.shape[0]
            w = image.shape[1]

            processed_image = cv2.resize(image, (self._args.model_height, self._args.model_width))

            # Finally, we'll define the main execution loop.  This will perform the following steps
            #
            # 1.  Preprocess the camera image
            # 2.  Execute the neural network
            # 3.  Parse the objects from the neural network output
            # 4.  Draw the objects onto the camera image
            # 5.  Convert the image to JPEG format and stream to the display widget

            data = self.preprocess(processed_image)
            cmap, paf = self._model_trt(data)
            cmap, paf = cmap.detach().cpu(), paf.detach().cpu()

            counts, objects, peaks = self._parse_objects(cmap, paf)  # , cmap_threshold=0.15, link_threshold=0.15)

            # We need to keep these data in case we want to draw the poses at some point
            detection_results.append(TensorRTBackend.DetectionResult(
                image=image,
                counts=counts,
                objects=objects,
                peaks=peaks
            ))

            # For each camera, generate a list of poses, containing the
            # pose_id (if available) and a dictionary of detected keypoint
            # coordinates, indexed by keypoint name.

            poses: List[Pose] = []
            for i in range(int(counts[0])):
                obj = objects[0][i]
                C = obj.shape[0]

                keypoints: Dict[str, Keypoint] = dict()
                for j in range(C):
                    k = int(obj[j])
                    if k >= 0:
                        peak = peaks[0][j][k]
                        x = round(float(peak[1]) * w)
                        y = round(float(peak[0]) * h)
                        cx = math.floor(float(peak[1]) * cmap[0][j].shape[1])
                        cy = math.floor(float(peak[0]) * cmap[0][j].shape[0])
                        c = float(cmap[0][j][cy][cx])
                        part = self._parts[j].upper()
                        keypoints[part] = Keypoint(
                            confidence=c,
                            part=part,
                            position=[x, y]
                        )

                pose_confidence = 0.0
                for keypoint in keypoints.values():
                    pose_confidence += keypoint.confidence
                if float(len(keypoints)) > 0:
                    pose_confidence = pose_confidence / float(len(keypoints))
                else:
                    pose_confidence = 0

                if pose_confidence < self._args.min_pose_confidence:
                    continue

                pose: Pose = Pose(
                    confidence=pose_confidence,
                    keypoints=keypoints,
                    id=i,
                    keypoints_definitions=self._keypoint_dict
                )

                poses.append(pose)

            keypoints_by_cam.append(PosesForCamera(
                poses=poses
            ))

        self._detection_results = detection_results
        self._poses_for_cameras = keypoints_by_cam

        return True

    def get_output(self) -> List[Channel]:
        channels: List[Channel] = [
            Channel(
                type=Channel.Type.POSE_2D,
                name="keypoints",
                data=self._poses_for_cameras,
                metadata={}
            )
        ]

        if self._tracked_images:
            for index, tracked_image in enumerate(self._tracked_images):
                channels.append(Channel(
                    type=Channel.Type.COLOR,
                    name=f"pose_image_{index}",
                    data=tracked_image,
                    metadata={
                        "resolution": [tracked_image.shape[1], tracked_image.shape[0]]
                    }
                ))

        if self._bounding_boxes is not None:
            channels.append(Channel(
                type=Channel.Type.BB_2D,
                name="bbox",
                data=self._bounding_boxes,
                metadata={}
            ))

        return channels

    def is_gpu_acceleration_active(self) -> bool:
        """Check if GPU acceleration is active."""
        if not torch.cuda.is_available() and not torch.backends.cuda.is_built():
            logger.warning("torch: CUDA not available")
            return False
        else:
            logger.info(f"torch: CUDA {torch.version.cuda} available")

        torch_cuda_device_count = 0

        # Testing CUDA enabled device count with cv2.cuda.getCudaEnabledDeviceCount which throws errors while torch.cuda.device_count does not:
        cv2_cuda_enabled_device_count = 0
        try:
            cv2_cuda_enabled_device_count = cv2.cuda.getCudaEnabledDeviceCount()
        except cv2.error as e:
            logger.error(f"{e.msg}")
            if e.code == -217:
                # -217: Gpu API call
                if e.err.find("forward compatibility was attempted on non supported HW") != -1 or e.err.find("system has unsupported display driver / cuda driver combination") != -1:
                    logger.error(
                        "This error may be due to a recent upgrade of Nvidia drivers, if so you may need to reboot your computer.")
                sys.exit(1)
        if cv2_cuda_enabled_device_count == 0:
            logger.warning("opencv: no CUDA devices detected")
            return False

        try:
            torch_cuda_device_count = torch.cuda.device_count()
            logger.info(f"torch: found {torch_cuda_device_count} device(s)")
        except:
            logger.error("torch: error")
        if torch_cuda_device_count == 0:
            logger.warning("torch: no CUDA devices detected")
            return False

        if not torch.backends.cudnn.enabled:
            logger.warning("torch: CUDNN not enabled")
            return False
        else:
            logger.info(f"torch: CUDNN {torch.backends.cudnn.m.version()} enabled")  # type: ignore

        return True

    def is_gpu_acceleration_needed(self) -> bool:
        """Check if GPU acceleration is needed."""
        return True
