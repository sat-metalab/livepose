# External Libraries
## SORT
All code in `livepose/sort.py` is taken from the
[SORT](//github.com/abewley/sort) library by
[Alex Bewley](https://alex.bewley.ai/).

SORT was initially described in
[this paper](https://arxiv.org/abs/1602.00763).

### License
Both LivePose and SORT are released under the GNU GPL v3.0 License.
See the included
[License](../../License.md)
file for more details.

### Citing
All research that uses LivePose (and therefore SORT) should please cite the SORT paper:
```
@inproceedings{Bewley2016_sort,
  author={Bewley, Alex and Ge, Zongyuan and Ott, Lionel and Ramos, Fabio and Upcroft, Ben},
  booktitle={2016 IEEE International Conference on Image Processing (ICIP)},
  title={Simple online and realtime tracking},
  year={2016},
  pages={3464-3468},
  keywords={Benchmark testing;Complexity theory;Detectors;Kalman filters;Target tracking;Visualization;Computer Vision;Data Association;Detection;Multiple Object Tracking},
  doi={10.1109/ICIP.2016.7533003}
}
```
