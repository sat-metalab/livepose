from typing import Any, Dict, Set

from livepose.dataflow import Channel, Flow, Stream
from livepose.output import register_output, Output
from livepose.filter import Filter

import json


@register_output("json")
class JSONOutput(Output):
    """
    Class which prints out filter results via JSON.
    """

    def __init__(self, **kwargs: Any):
        super(JSONOutput, self).__init__("json", **kwargs)

    def add_parameters(self) -> None:
        self._parser.add_argument("--path", type=str, default="./livepose-output.json", help="output file")
        self._parser.add_argument("--header", type=str, default="{\n[\n", help="header")
        self._parser.add_argument("--footer", type=str, default="\n]\n}", help="footer")

    def init(self) -> None:
        self._file = open(self._args.path, 'w')
        self._file.write(self._args.header)
        self._frameNum: int = 0

    def send_flow(self, flow: Flow, now: float, dt: float) -> None:
        """
        """
        output_streams = flow.get_streams_by_type(Stream.Type.FILTER)

        for stream in output_streams.values():
            channels = stream.get_channels_by_type(Channel.Type.OUTPUT)
            for channel in channels:
                results: Dict[str, Any] = channel.data

                if self._frameNum > 0:
                    self._file.write(",")

                json.dump(results, self._file, indent=4)

            self._frameNum += 1

    def stop(self) -> None:
        self._file.write(self._args.footer)
