import cv2
import pyrealsense2 as rs

from livepose.dataflow import Channel
from livepose.camera import register_camera, Camera
from typing import Any, List, Optional, Tuple

import logging
import numpy as np
import sys

logger = logging.getLogger(__name__)


@register_camera("pyrealsense2")
class PyRealSense2Camera(Camera):
    """
    Utility class embedding a camera with pyrealsense2, its parameters and buffers
    """

    COLOR_METADATA = "pyrealsense.color"
    DEPTH_METADATA = "pyrealsense.depth"
    DEPTH_SCALE_METADATA = "pyrealsense.depth_scale"

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()
        self._parser.add_argument("--id", type=int, default=0, help="Camera id")
        self._parser.add_argument("--align-frames", type=bool, default=True, help="Align frames")
        self._parser.add_argument("--loop-playback", type=bool, default=False, help="Loop playback")
        self._parser.add_argument("--realtime-playback", type=bool, default=False, help="Realtime playback")

    def init(self) -> None:

        self._color_frame: Optional[rs.frame] = None
        self._depth_frame: Optional[rs.depth_frame] = None

        self._has_depth = True
        self._intrinsics: Optional[rs.intrinsics] = None

        # Get a list of all RS cameras connected
        # We connect to them through their ID, the path is merely a name
        rs2_ctx = rs.context()
        devices = rs2_ctx.query_devices()
        if not self._args.path.endswith(".bag") and len(devices) < self._args.id + 1:
            logging.error(f"No camera with id {self._args.id} detected")
            exit(0)

        self._pipeline = rs.pipeline()
        self._config = rs.config()

        # If the source is a ROSbag file
        if self._args.path.endswith(".bag"):
            rs.config.enable_device_from_file(self._config, self._args.path, repeat_playback=self._args.loop_playback)
        # else if it seems to be a real camera
        else:
            device = devices[self._args.id]
            self._config.enable_device(device.get_info(rs.camera_info.serial_number))

            found_rgb = False
            for s in device.sensors:
                if s.get_info(rs.camera_info.name) == 'RGB Camera':
                    found_rgb = True
                    break
            if not found_rgb:
                logging.error("pyrealsense2 api requires a depth camera with color sensor")
                exit(0)

            self._config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)

            # For the color stream, set format to RGBA
            # To allow blending of the color frame on top of the depth frame
            # self._config.enable_stream(rs.stream.color, 640, 480, rs.format.rgba8, 30)
            self._config.enable_stream(rs.stream.color, 320, 240, rs.format.bgr8, 30)

        self._profile = self._pipeline.start(self._config)

        if self._args.path.endswith(".bag"):
            # Set real time file playback:
            # on: plays at recorded framerate, some frames get skipped in the beginning
            # off: plays as fast as possible
            playback = self._profile.get_device().as_playback()
            playback.set_real_time(self._args.realtime_playback)

            # Check whether a depth stream is available
            try:
                self._profile.get_stream(rs.stream.depth)
            except RuntimeError:
                self._has_depth = False

        if self._has_depth:
            # Our goal is to generate depth without any holes, since these are going to pose an immediate problem to our algorithm. The best way to reduce the number of missing pixels is by letting the hardware do it.
            # The D400 cameras have a High Density preset we can take advantage of.
            # https://github.com/IntelRealSense/librealsense/tree/master/examples/measure#converting-between-pixels-and-points-in-3d
            depth_sensor = self._profile.get_device().first_depth_sensor()
            preset_range = depth_sensor.get_option_range(rs.option.visual_preset)
            for i in range(int(preset_range.max)):
                visual_preset = depth_sensor.get_option_value_description(rs.option.visual_preset, i)
                if visual_preset == "High Accuracy":
                    depth_sensor.set_option(rs.option.visual_preset, i)
                    logging.info("Enabled High Accuracy visual preset")

            # Some parameters from the cameras are useful for subsequent stages
            # Let's get them to be stored as metadata
            self._depth_scale = depth_sensor.get_depth_scale()

            # Create an align object to spatially align all streams to depth viewport
            # We do this because:
            #   a. Usually depth has wider FOV, and we only really need depth for this demo
            #   b. We don't want to introduce new holes
            align_to = rs.stream.depth
            self._align = rs.align(align_to)

        # Generate extrinsic/intrinsic parameters
        self.kmat
        self.extrinsics

    @Camera.kmat.getter  # type: ignore
    def kmat(self) -> Optional[np.array]:
        """
        Get the intrinsics parameters of the device
        :return: np.array - K matrix
        """
        if not self._has_depth:
            return self._kmat

        if self._kmat is None:
            #self._intrinsics = self._depth_frame.profile.as_video_stream_profile().get_intrinsics()
            self._intrinsics = self._profile.get_stream(rs.stream.depth).as_video_stream_profile().get_intrinsics()

            # Transform camera matrix from pyrealsense2 to OpenCV:
            # https://github.com/IntelRealSense/librealsense/wiki/Projection-in-RealSense-SDK-2.0#intrinsic-camera-parameters
            # https://github.com/opencv/opencv/blob/4.5.3/modules/calib3d/include/opencv2/calib3d.hpp#L77
            # The camera intrinsic matrix \f$A\f$ is composed of the focal lengths \f$f_x\f$ and \f$f_y\f$, which are
            # expressed in pixel units, and the principal point \f$(c_x, c_y)\f$, that is usually close to the
            # image center:
            # \f[A = \vecthreethree{f_x}{0}{c_x}{0}{f_y}{c_y}{0}{0}{1},\f]
            self._kmat = np.array([
                [self._intrinsics.fx, 0.0, self._intrinsics.ppx],
                [0.0, self._intrinsics.fy, self._intrinsics.ppy],
                [0.0, 0.0, 1.0]
            ])
        return self._kmat

    @Camera.extrinsics.getter  # type: ignore
    def extrinsics(self) -> Optional[np.array]:
        """
        Get the extrinsics parameters of the device
        :return: np.array - extrinsics parameters
        """
        if not self._has_depth:
            return self._extrinsics

        if self._extrinsics is None:
            # Concatenate extrinsic camera parameters:
            # https://github.com/IntelRealSense/librealsense/wiki/Projection-in-RealSense-SDK-2.0#extrinsic-camera-parameters

            #extrinsics = self._depth_frame.profile.get_extrinsics_to(self._color_frame.profile)
            extrinsics = self._profile.get_stream(rs.stream.depth).get_extrinsics_to(
                self._profile.get_stream(rs.stream.color))

            r = np.array(extrinsics.rotation).reshape(3, 3).T
            t = np.array(extrinsics.translation).reshape(3, 1)
            self._extrinsics = np.concatenate((r, t), axis=1)
            # Note: if depth and color streams are aligned, extrinsic translation will be a (3,1) zero matrix and rotation a (3,3) identity matrix.

        return self._extrinsics

    @property
    def intrinsics(self) -> Optional[rs.intrinsics]:
        """
        Get the intrinsics parameters of the realsense device
        Doc: https://github.com/IntelRealSense/librealsense/wiki/Projection-in-RealSense-SDK-2.0#intrinsic-camera-parameters
        :return: rs.intrinsics - intrinsics parameters
        """
        return self._intrinsics

    @property
    def fps(self) -> float:
        """
        Get the grab framerate for the camera
        :return: float - Framerate as frames per second
        """
        profile = self._pipeline.get_active_profile()
        stream_profile = profile.get_stream(rs.stream.color)
        video_stream_profile = stream_profile.as_video_stream_profile()
        fps = video_stream_profile.fps()
        return fps

    @fps.setter
    def fps(self, framerate: float) -> None:
        """
        Set the grab framerate, in frames per seconde
        :param: float - Framerate
        """
        if(self.fps != framerate):
            profile = self._pipeline.get_active_profile()
            stream_profile = profile.get_stream(rs.stream.color)
            video_stream_profile = stream_profile.as_video_stream_profile()

            # Check if new settings are compatible with device
            pipeline_wrapper = rs.pipeline_wrapper(self._pipeline)
            pipeline_profile = self._config.resolve(pipeline_wrapper)
            device = pipeline_profile.get_device()
            sensor = device.first_color_sensor()
            stream_profiles = sensor.get_stream_profiles()
            supported = False
            for stream_profile in stream_profiles:
                supported_video_stream_profile = stream_profile.as_video_stream_profile()
                if (supported_video_stream_profile.width(), supported_video_stream_profile.height(), supported_video_stream_profile.fps()) == (video_stream_profile.width(), video_stream_profile.height(), framerate):
                    supported = True
                    break

            if supported:
                self.close()
                # Do not first disable the color stream, otherwise RuntimeError: Profile does not contain the requested stream
                # self._config.disable_stream(rs.stream.color)
                self._config.enable_stream(
                    rs.stream.color,
                    video_stream_profile.width(),
                    video_stream_profile.height(),
                    video_stream_profile.format(),
                    int(framerate)
                )
                self.open()

            else:
                logging.error(
                    f"Can not change fps, camera color streaming profile {video_stream_profile.width()}x{video_stream_profile.height()} @ {framerate} {framerate,video_stream_profile.format().name} unsupported, exiting")
                self.close()
                exit(1)

    @property
    def resolution(self) -> Tuple[int, int]:
        """
        Get the resolution of the device
        :return: Tuple[int, int]
        """
        profile = self._pipeline.get_active_profile()
        stream_profile = profile.get_stream(rs.stream.color)
        video_stream_profile = stream_profile.as_video_stream_profile()
        width = video_stream_profile.width()
        height = video_stream_profile.height()
        return (width, height)

    @resolution.setter
    def resolution(self, res: Tuple[int, int]) -> None:
        """
        Set the resolution of the device
        :param: Tuple[int, int]
        """
        if(self.resolution != res):
            profile = self._pipeline.get_active_profile()
            stream_profile = profile.get_stream(rs.stream.color)
            video_stream_profile = stream_profile.as_video_stream_profile()

            # Check if new settings are compatible with device
            pipeline_wrapper = rs.pipeline_wrapper(self._pipeline)
            pipeline_profile = self._config.resolve(pipeline_wrapper)
            device = pipeline_profile.get_device()
            sensor = device.first_color_sensor()
            stream_profiles = sensor.get_stream_profiles()
            supported = False
            for stream_profile in stream_profiles:
                supported_video_stream_profile = stream_profile.as_video_stream_profile()
                if (supported_video_stream_profile.width(), supported_video_stream_profile.height(), supported_video_stream_profile.fps()) == (res[0], res[1], video_stream_profile.fps()):
                    supported = True
                    break

            if supported:
                self.close()
                # Do not first disable the color stream, otherwise RuntimeError: Profile does not contain the requested stream
                # self._config.disable_stream(rs.stream.color)
                self._config.enable_stream(
                    rs.stream.color,
                    int(res[0]),
                    int(res[1]),
                    video_stream_profile.format(),
                    video_stream_profile.fps()
                )
                self.open()
            else:
                logging.error(
                    f"Can not change resolution, camera color streaming profile {res[0]}x{res[1]} @ {video_stream_profile.fps()} {video_stream_profile.format().name} unsupported, exiting")
                self.close()
                exit(1)

    def is_open(self) -> bool:
        """
        Check if a video capturing device has been initialized already
        :return: bool
        """
        return self._profile is not None

    def grab(self) -> bool:
        """
        Grabs the next frame from video file or capturing device
        :return: bool - in the case of success
        """
        self._color_frame = None
        self._depth_frame = None

        while (self._depth_frame is None and self._has_depth) or self._color_frame is None:
            if self._args.path.endswith(".bag"):
                playback = self._profile.get_device().as_playback()
                if not self._args.loop_playback and playback.current_status() == rs.playback_status.stopped:
                    logger.info(f"pyrealsense2 api: reached end of bag file {self._args.path}")
                    try:
                        self._pipeline.stop()
                    except RuntimeError as e:
                        logger.info(f"pyrealsense2 api: {e}")
                    self._profile = None
                    return False

            frames = self._pipeline.wait_for_frames()

            # Align the depth frame to color frame
            if self._args.align_frames:
                frames = self._align.process(frames)

            # Get aligned frames
            if self._has_depth:
                self._depth_frame = frames.get_depth_frame()
            self._color_frame = frames.get_color_frame()

        if self._has_depth:
            return self._depth_frame is not None and self._color_frame is not None
        else:
            return self._color_frame is not None

    def retrieve(self) -> bool:
        """
        Decodes and set the grabbed video frame
        grab() should be called beforehand
        :return: bool - true in the case of success
        """
        if self._has_depth:
            if self._depth_frame is not None and self._color_frame is not None:
                self._frame = self._color_frame
                return True
            return False
        else:
            if self._color_frame is not None:
                self._frame = self._color_frame
                return True
            return False

    def get_raw_channels(self) -> List[Channel]:
        """
        Get the raw channels from the camera, without any
        transformation (flip, rotate, etc) applied
        :return: List[Channel] - List of raw channels
        """
        channels: List[Channel] = []

        if self._color_frame is not None:
            if self._color_frame.profile.format() == rs.format.yuyv:
                video_frame = self._color_frame.as_video_frame()
                data = np.asanyarray(self._color_frame.get_data(), dtype=np.uint16)
                data = data.view(np.uint8).reshape((
                    video_frame.height,
                    video_frame.width,
                    2
                ))
                color_data = cv2.cvtColor(data, cv2.COLOR_YUV2BGR_YUY2)
            else:
                color_data = np.asanyarray(self._color_frame.get_data())

            channels.append(Channel(
                type=Channel.Type.COLOR,
                name=self._args.path,
                data=color_data,
                metadata={
                    PyRealSense2Camera.COLOR_METADATA: self._color_frame
                }
            ))

        if self._depth_frame is not None:
            channels.append(Channel(
                type=Channel.Type.DEPTH,
                name=self._args.path,
                data=np.asanyarray(self._depth_frame.get_data()),
                metadata={
                    PyRealSense2Camera.DEPTH_METADATA: self._depth_frame,
                    PyRealSense2Camera.DEPTH_SCALE_METADATA: self._depth_scale
                }
            ))

        return channels

    def open(self) -> bool:
        """
        Open video file or device
        :return: bool - true if successful
        """
        self._profile = self._pipeline.start(self._config)
        return self._profile is not None

    def close(self) -> None:
        """
        Closes video file or capturing device
        """
        self._profile = self._pipeline.stop()

    @property  # type: ignore
    def rotate(self) -> int:
        """
        Get the status of rotation (clockwise)
        :return: int - rotation
        """
        return self._args.rotate

    @rotate.setter  # type: ignore
    def rotate(self, rotate: int) -> None:
        """
        Set camera rotation (clockwise)
        :param: int - rotation
        """
        self._args.rotate = rotate
