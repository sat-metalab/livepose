.. LivePose documentation main file, created by
   sphinx-quickstart on Fri Jan 24 10:05:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

LivePose API documentation
=====================

`LivePose <https://sat-mtl.gitlab.io/tools/livepose>`_ is a command line tool which tracks people skeletons from a video feed (live or not), and sends the resulting measurements through OSC. It can also apply some filtering on the skeletons to extract some more information, and send the result through along with the skeletons.
 

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   livepose/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
