# Config Specification
LivePose uses JSON files for configuring various options and settings.

Each section below represents a top-level property of the JSON config file.
Each top-level section is optional; if one is not included the defaults will be
used, as specified below.

## Contents
[[_TOC_]]

## Example Config File
A full config file with all default parameters explicitly set is included in
this repo: [config/default.json](config/default.json).
This is the config file used by default when running LivePose.

Additional examples can be found in the [config/](config/) directory.

## Backend
Each config file may have a `backend` property, which can be used to configure
the deep learning model, or "backend", used for inference and pose estimation.
Within the backend property you can specify several deep learning parameters
which can have varying effects on performance speed and accuracy.

### Name
The `backend` object must contain a `name` property. This is the name of the
deep learning model to be used for estimating poses. It can be one of:
- `"mediapipe"` (recommended for generic hardware)
- `"posenet"`
- `"mmpose"`
- `"trt"` (recommended for Nvidia GPUs)
- `"void"`

Example:
```bash
{
  "backend": {
    "name": "mediapipe"
  }
}
```
### Comparison

| Backend Name  | Source                                                                       | Required Architecture        | Accuracy     | Speed   |
|---------------|------------------------------------------------------------------------------|------------------------------|--------------|---------|
| `"mediapipe"` | ([more info](https://github.com/google/mediapipe))                           | CPU or GPU                   | Variable (*) | High    |
| `"mmpose"`    | ([more info](https://github.com/open-mmlab/mmpose))                          | CPU or GPU                   | Variable (*) | Low     |
| `"posenet"`   | ([more info](https://github.com/tensorflow/tfjs-models/tree/master/posenet)) | CPU or GPU                   | Variable (*) | Medium  |
| `"trt"`       | ([more info](https://gitlab.com/sat-mtl/tools/forks/trt_pose))               | Nvidia GPU with CUDA >= 10.2 | Variable (*) | **Highest** |
| `"void"`      | /                                                                            | /                            | /            | /       |

(*) Accuracy is marked as variable as it depends on chosen models, backbones and datasets; defined in configuration parameters (explained in next section).

In summary:
- `"mediapipe"` is recommended as a default on generic hardware because of its speed and adaptability.
- `"mmpose"` is recommended for exploring an extensive collection of pose estimation algorithms, including results from the newest research works.
- `"posenet"` was one of the first backend implemented in LivePose, now superseded by `"mediapipe"` as a default on generic hardware.
- `"trt"` is recommended as a default on NVIDIA hardware (including Jetson boards) it is optimized for, using [TensorRT](https://github.com/NVIDIA/TensorRT/).
- `"void"` is mainly useful for testing the performance of other LivePose components, as it does not perform any deep learning inference or pose estimation.

### Params
The `backend` object may contain a `params` property, which is a dictionary of
deep learning parameters. The parameters available are different for the
various backends.

#### MediaPipe Params

| Parameter Name              | Type / Possible Values           | Default Value | Description |
|-----------------------------|----------------------------------|---------------|-------------|
| `"detect_faces"` | `bool` | `False` | Whether to detect faces or not. |
| `"detect_face_meshs"` | `bool` | `False` | Whether to detect face meshs or not. |
| `"detect_hands"` | `bool` | `True` | Whether to detect hands or not. |
| `"detect_poses"` | `bool` | `False` | Whether to detect poses or not. |
| `"detect_holistic"` | `bool` | `False` | Whether to use holistic detection (face meshs and hands and poses) or not. |
| `"use_gpu"` | `bool` | `True` | Whether to use GPU or not. |
| `"min_detection_confidence"` | `float` | `0.5` | Minimum detection confidence. |

#### PoseNet Params
Here is a table of the various PoseNet parameters and their defaults.

| Parameter Name              | Type / Possible Values           | Default Value | Description |
|-----------------------------|----------------------------------|---------------|-------------|
| `"base_model"`              | `"mobilenet"` or `"resnet50"`    | `"mobilenet"` | Base model used for inference. Mobilenet is faster but less accurate, resnet is more accurate but slower |
| `"device_id"`               | `int`                            | `-1`          | CUDA device id to use if CUDA is enabled. If set to `-1`, the system default device will be used. |
| `"draw_bounding_boxes"`     | `bool`                           | `false`       | Whether or not to display bounding boxes overlayed on top of poses. Bounding boxes are computed for each pose using all detected keypoints, regardless of confidence score. |
| `"draw_skeleton_edges"`     | `bool`                           | `true`        | Whether or not to display edges between keypoints when showing results. If set to `false`, only keypoints are shown |
| `"input_resolution"`        | `[int, int]`                     | `[500, 500]`  | Images will be scaled to this size before being passed to the model for inference. Larger values will give higher accuracy but slower performance |
| `"min_keypoint_confidence"` | `float`, between `0.` and `1.`   | `0.1`         | Minimum confidence score required for detected keypoints to be displayed. Note, keypoints with a score below this threshold will not be displayed on screen, but will still be included in other output formats such as OSC and WebSocket. Keypoints below the threshold will also not be used as roots in the multi pose detection algorithm (see multi pose detection code for more details). |
| `"min_pose_confidence"`     | `float`, between `0.` and `1.`   | `0.15`        | Same as above, but corresponds to the confidence score of an entire Pose. Poses with a score below this threshold will not be displayed on screen AND will not be included in any other output formats. These poses will simply be discarded during the multi pose detection algorithm. Note, this value is ignored if `multi_pose_detection` is set to `false`.
| `"multi_pose_detection"`    | `bool`                           | `true`        | Whether to use the single or multi pose detection algorithm. If you know only one person will be in the frame, the single pose algorithm is simpler and faster. When in doubt, use the multi pose algorithm if you do not know how many people will be in the frame. |
| `"output_stride"`           | `int`, one of `8`, `16`, or `32` | `16`          | Image scaling factor used by the PoseNet algorithm during inference. A higher stride (more compressed image) will give faster performance but lower accuracy, and vice versa. Note, the `"mobilenet"` base model only allows for strides `8` and `16`; `"resnet50"` only allows `16` and `32`. |

Here is a full example with the default parameters:
```bash
{
  "backend": {
    "name": "posenet",
      "params": {
        "base_model": "mobilenet",
        "device_id": 0,
        "draw_bounding_boxes": false,
        "draw_skeleton_edges": true,
        "input_resolution": [500, 500],
        "min_keypoint_confidence": 0.1,
        "min_pose_confidence": 0.15,
        "multi_pose_detection": true,
        "output_stride": 16
      }
    }
}
```

#### MMPose Params
Here is a table of the various `"mmpose"` parameters and their defaults.

| Parameter Name        | Type / Possible Values | Default Value                                        | Description |
| --------------------- | ---------------------- | ---------------------------------------------------- | ----------- |
| `"cam_id"` | `str` | `0` | None |
| `"det_config"` | `str` | `external/mmpose/demo/mmdetection_cfg/ssdlite_mobilenetv2_scratch_600e_coco.py` | Config file for detection |
| `"enable_detection"` | `int` | `1` | Enable detection of entities (persons, animals...) containing keypoints, represented with bounding boxes and labels |
| `"det_checkpoint"` | `str` | `https://download.openmmlab.com/mmdetection/v2.0/ssd/ssdlite_mobilenetv2_scratch_600e_coco/ssdlite_mobilenetv2_scratch_600e_coco_20210629_110627-974d9307.pth` | Checkpoint file for detection |
| `"enable_human_pose"` | `int` | `1` | Enable human pose estimation |
| `"enable_animal_pose"` | `int` | `0` | Enable animal pose estimation |
| `"human_pose_config"` | `str` | `external/mmpose/configs/wholebody/2d_kpt_sview_rgb_img/topdown_heatmap/coco-wholebody/vipnas_res50_coco_wholebody_256x192_dark.py` | Config file for human pose |
| `"human_pose_checkpoint"` | `str` | `https://download.openmmlab.com/mmpose/top_down/vipnas/vipnas_res50_wholebody_256x192_dark-67c0ce35_20211112.pth` | Checkpoint file for human pose |
| `"human_det_ids"` | `list` | `[1]` | Object category label of human in detection results.Default is [1(person)], following COCO definition. |
| `"animal_pose_config"` | `str` | `external/mmpose/configs/animal/2d_kpt_sview_rgb_img/topdown_heatmap/animalpose/hrnet_w32_animalpose_256x256.py` | Config file for animal pose |
| `"animal_pose_checkpoint"` | `str` | `https://download.openmmlab.com/mmpose/animal/hrnet/hrnet_w32_animalpose_256x256-1aa7f075_20210426.pth` | Checkpoint file for animal pose |
| `"animal_det_ids"` | `list` | `[16, 17, 18, 19, 20]` | Object category label of animals in detection resultsDefault is [16(cat), 17(dog), 18(horse), 19(sheep), 20(cow)], following COCO definition. |
| `"device"` | `str` | `cuda:0` | Device used for inference |
| `"det_score_thr"` | `float` | `0.5` | bbox score threshold |
| `"kpt_thr"` | `float` | `0.3` | bbox score threshold |
| `"vis_mode"` | `int` | `2` | 0-none. 1-detection only. 2-detection and pose. |
| `"sunglasses"` | `bool` | `False` | Apply `sunglasses` effect. |
| `"bugeye"` | `bool` | `False` | Apply `bug-eye` effect. |
| `"out_video_file"` | `NoneType` | `None` | Record the video into a file. This may reduce the frame rate |
| `"out_video_fps"` | `int` | `20` | Set the FPS of the output video file. |
| `"buffer_size"` | `int` | `-1` | Frame buffer size. If set -1, the buffer size will be automatically inferred from the display delay time. Default: -1 |
| `"inference_fps"` | `int` | `10` | Maximum inference FPS. This is to limit the resource consuming especially when the detection and pose model are lightweight and very fast. Default: 10. |
| `"display_delay"` | `int` | `0` | Delay the output video in milliseconds. This can be used to align the output video and inference results. The delay can be disabled by setting a non-positive delay time. Default: 0 |
| `"synchronous_mode"` | `bool` | `False` | Enable synchronous mode that video I/O and inference will be temporally aligned. Note that this will reduce the display FPS. |

##### Examples

We provide 2 configuration examples:
- raised by [this question](https://github.com/open-mmlab/mmpose/issues/67#issue-679060358):
> Does MMPose support a temporal analysis of videos yet, where the pose estimation is not done on a frame-by-frame basis, but rather using temporal information (...) ?
- and suggested in [this reply](https://github.com/open-mmlab/mmpose/issues/67#issuecomment-1007877526):
> We currently support 3D video method (checkout [here](https://mmpose.readthedocs.io/en/latest/topics/body%282d%2Ckpt%2Csview%2Cvid%29.html)) VideoPose3D (CVPR'19) and 2D video method (checkout [here](https://mmpose.readthedocs.io/en/latest/topics/body%282d%2Ckpt%2Csview%2Cvid%29.html)) PoseWarper (NeurIPS'19).

Note that these examples are still works in progress.

###### PoseWarper (NeurIPS 2019)

- starting point in mmpose doc:
[Body(2D,Kpt,Sview,Vid)](https://mmpose.readthedocs.io/en/latest/topics/body%282d%2Ckpt%2Csview%2Cvid%29.html)
corresponding to
[external/mmpose/configs/body/2d_kpt_sview_rgb_vid/README.md](../external/mmpose/configs/body/2d_kpt_sview_rgb_vid/README.md)
- `"human_pose_config"` (containing subpath `2d_kpt_sview_rgb_vid`) and `"human_pose_checkpoint"` are based on this mmpose config:
[external/mmpose/configs/body/2d_kpt_sview_rgb_vid/posewarper/README.md](../external/mmpose/configs/body/2d_kpt_sview_rgb_vid/posewarper/README.md)
leading to
[external/mmpose/configs/body/2d_kpt_sview_rgb_vid/posewarper/posetrack18/hrnet_posetrack18_posewarper.md](../external/mmpose/configs/body/2d_kpt_sview_rgb_vid/posewarper/posetrack18/hrnet_posetrack18_posewarper.md)
- `"enable_detection"` needs to be deactivated until a matching configuration becomes available, in [external/mmpose/demo/mmdetection_cfg](../external/mmpose/demo/mmdetection_cfg) or [mmdetection/configs](https://github.com/open-mmlab/mmdetection/tree/master/configs)

```bash
{
  "backend": {
    "name": "mmpose",
    "params": {
      "enable_human_pose": 1,
      "human_pose_config": "external/mmpose/configs/body/2d_kpt_sview_rgb_vid/posewarper/posetrack18/hrnet_w48_posetrack18_384x288_posewarper_stage2.py",
      "human_pose_checkpoint": "https://download.openmmlab.com/mmpose/top_down/posewarper/hrnet_w48_posetrack18_384x288_posewarper_stage2-4abf88db_20211130.pth",
      "enable_detection": 0
    }
  }
}
```
###### VideoPose3D (CVPR 2019)

- starting point in mmpose doc:
[Body(3D,Kpt,Sview,Vid)](https://mmpose.readthedocs.io/en/latest/topics/body%282d%2Ckpt%2Csview%2Cvid%29.html)
corresponding to
[external/mmpose/configs/body/3d_kpt_sview_rgb_vid/README.md](../external/mmpose/configs/body/3d_kpt_sview_rgb_vid/README.md)
- `"human_pose_config"` (containing subpath `3d_kpt_sview_rgb_vid`) and `"human_pose_checkpoint"` are based on this mmpose config:
[external/mmpose/configs/body/3d_kpt_sview_rgb_vid/video_pose_lift/README.md](../external/mmpose/configs/body/3d_kpt_sview_rgb_vid/video_pose_lift/README.md)
leading to
[external/mmpose/configs/body/3d_kpt_sview_rgb_vid/video_pose_lift/h36m/videopose3d_h36m.md](../external/mmpose/configs/body/3d_kpt_sview_rgb_vid/video_pose_lift/h36m/videopose3d_h36m.md)
- `"enable_detection"` needs to be deactivated until a matching configuration becomes available, in [external/mmpose/demo/mmdetection_cfg](../external/mmpose/demo/mmdetection_cfg) or [mmdetection/configs](https://github.com/open-mmlab/mmdetection/tree/master/configs)

```bash
{
  "backend": {
    "name": "mmpose",
    "params": {
        "enable_human_pose": 1,
        "human_pose_config": "external/mmpose/configs/body/3d_kpt_sview_rgb_vid/video_pose_lift/h36m/videopose3d_h36m_243frames_fullconv_supervised.py",
        "human_pose_checkpoint": "https://download.openmmlab.com/mmpose/body3d/videopose/videopose_h36m_243frames_fullconv_supervised-880bea25_20210527.pth",
        "enable_detection": 0
    }
  }
}
```

#### Trt Params
Here is a table of the various `"trt"` parameters and their defaults.

| Parameter Name        | Type / Possible Values | Default Value                                        | Description |
| --------------------- | ---------------------- | ---------------------------------------------------- | ----------- |
| `"keypoint_dataset"`  | `str`                  | `"BODY_25"`                                          | Identifier of keypoint dataset. Current options are: `"BODY_25"`, `"FACE_70"`, `"HAND_21"`.  |
| `"model_path"`        | `str`                  | `"livepose/models/trt"`                              | Path where model weights and description files are stored. |
| `"model_desc"`        | `str`                  | `"human_pose.json"`                                  | JSON file which describes the human pose task in [COCO](https://cocodataset.org) format, it is the category descriptor pulled from the annotations file. |
| `"model_height"`      | `int`                  | `224`                                                | Input height that the model was trained with. |
| `"model_width"`       | `int`                  | `224`                                                | Input width that the model was trained with. |
| `"model_weights"`     | `str`                  | `"resnet18_baseline_att_224x224_A_epoch_249.pth"`    | Model weights filename. |
| `"model_optimized"`   | `str`                  | `"resnet18_baseline_att_224x224_A_epoch_249_trt.pth"`| Optimized model weights filename. This file will be automatically created by LivePose and re-generated when required by dependencies updates. We recommend to name it similarly to `"model_weights"`, with `_trt.pth` suffix. |
| `"optimize"`          | `bool`                 | `False`                                              | Whether to force re-optimizing the model or not. |

Example:
```bash
{
  "backend": {
        "name": "trt",
        "params": {
            "keypoint_dataset": "BODY_25",
            "model_path": "livepose/models/trt",
            "model_desc": "human_pose.json",
            "model_weights": "resnet18_baseline_att_224x224_A_epoch_249.pth",
            "model_optimized": "resnet18_baseline_att_224x224_A_epoch_249_trt.pth",
            "model_height": 224,
            "model_width": 224,
            "optimize": false
        }
    }
}
```
## Cameras
Each config file may have a `cameras` property, which can be used to specify
parameters for the camera(s).

### Input paths
`LivePose` must run its inference on an image or a video stream.
The `"input_paths"` allows to specify the path to the image file or camera.
It takes the values as a list, `[str]`, supporting multiple video streams in "parallel".
When an image file is given as a path, add the `--wait` or `-w` option to
prevent automatic refresh of the input.
To run `LivePose`, it would then be: `./bin/livepose --wait`

Example:
```bash
{
  "cameras": {
    "input_paths": ["/dev/video0", "/dev/video2"]
  }
}
```

Note: the `"pyrealsense2"` api supports rosbag files as `"input_paths"`.

### Params
The `cameras` section may contain a `params` property, which is a dictionary of
camera parameters. The `"input_paths"` of the cameras are the dictionary keys,
with each value being the params for the corresponding camera.

| Parameter Name              | Type / Possible Values           | Default Value | Description |
|-----------------------------|----------------------------------|---------------|-------------|
| `"resolution"`              | `[int,int']`                     | `None`        | Desired resolution for the camera. If no values are provided, it will take the default resolution of the camera. |
|`"framerate"`                | `int`                            | `None`        | Desired framerate for the camera. If no value are set, it will take the default value of the camera. |
|`"api"`                      | `string`                         | `None`        | Desired API for the camera. Options: `"opencv"` (default, recommended by default, supports many cameras and [APIs](https://github.com/opencv/opencv/blob/4.5.3/modules/videoio/include/opencv2/videoio.hpp#L90) built in at compilation time and auto-detected at runtime) `"pyrealsense2"` (requires an Intel RealSense camera) or `"ndi"` |
|`"align_frames"`             | `bool`                           | `True`        | Align color/depth frames or not. Required for 3D position estimation, but not for 2D pose estimation. Speeds up framerate when inactive. Only valid with `"pyrealsense2"` api. |
|`"loop_playback"`            | `bool`                           | `False`       | Loop rosbag file playback. Only valid with `"pyrealsense2"` api and rosbag files as `"input_paths"`. |
|`"realtime_playback"`        | `bool`                           | `False`       | Sets realtime playback. If `True`, rosbag file is played back at recorded framerate, some frames may get skipped in the beginning. If `False`, rosbag file is played back as fast as possible. Only valid with `"pyrealsense2"` api and rosbag files as `"input_paths"`. |

Examples:
- with default opencv api
```bash
"cameras": {
  "input_paths": ["/dev/video0"],
  "params": {
    "/dev/video0": {
      "resolution": [640, 480],
      "framerate": 60
    }
  }
}
```
- with `"pyrealsense2"` api
```bash
"cameras": {
  "input_paths": ["file.bag"],
  "params": {
    "file.bag": {
      "api": "pyrealsense2",
      "align_frames": false,
      "loop_playback": true,
      "realtime_playback": false
    }
  }
}
```

- with `"ndi"` api
```json
    "cameras": {
        "input_paths":["your ndi stream name"],
        "params": {
            "your ndi stream name": {
                "api": "ndi"
            }
        }
    }
```

### Flip camera
Flip images around the y-axis (i.e. mirrored horizontally) for all cameras.
Ideal for use with webcams. Its default value is false.

Example:
```bash
{
  "cameras": {
    "input_paths": ["/dev/video0"],
    "flip_camera": true
  }
}
```

### Rotate camera
-------
Rotate images clockwise around their center for all cameras.
Ideal for use with vertically tilted webcams. Its default value is 0.
Values that can be passed as parameter are 0, 90, 180 and 270.

Example:
```bash
{
  "cameras": {
    "input_paths": ["/dev/video0"],
    "rotate_camera": 90
  }
}
```

### Calibration Parameters
`LivePose` accepts calibration parameters for cameras.
These calibration options can be used to remove distortion from the video stream,
or with filters which require a calibrated camera, such as the
`triangulation filter` or `position filter`.

When calibrating a camera, three matrices are used. One representing the
intrinsic parameters of the camera, in our case called `intrinsics`.
The second, which we call `extrinsics`, represents the extrinsics parameters of
the camera like the position and orientation in space.
Finally, the distortion coefficients matrice, `distortion_coeffs`, encodes the
camera lens distortion coefficients.
`LivePose` accepts distortion matrices with 4 or 5 coefficients.
To calibrate your camera, we provide two scripts in the
[tools/](tools/) directory.

See the example below to see how to provide these matrices to `LivePose`.
Note that each matrice type expects a dictionary with the key being the camera path.
```bash
"cameras": {
        "input_paths":["/dev/video0"],
        "intrinsics": {
            "/dev/video0":
            [
                1191.2388997884193,
                0.0,
                392.99345721315467,
                0.0,
                1369.4716249031615,
                294.6838595383168,
                0.0,
                0.0,
                1.0
            ]
        },
        "distortion_coeffs": {
            "/dev/video0":
            [
                -0.516787186976822,
                -62.72111629425378,
                0.0,
                0.0,
                477.0063074126019
            ]
        },
        "extrinsics": {
            "/dev/video0":
            [
                0.9875441485524081,
                0.1532494019295864,
                -0.035653547763488405,
                0.24167190663180213,
                -0.1081219883008355,
                0.8255876943104596,
                0.5538181963866962,
                -0.21805334438810126,
                0.11430743766602965,
                -0.5430649867293631,
                0.8318739266757094,
                5.84098596414546
            ]
        }
}
```

## Filters
Each config file may have a `filters` property, which can be used for
post-processing of raw pose estimation results, following the inference step.
Each filter receives the pose data for each camera as input, and outputs
the filtered results to the desired output stream.

### Summary of the available filters and their outputs

| Filter name    | Output     |
|----------------|------------|
| `skeletons`    |`{camera number}/{detected person}/{Body Part} {screen X position} {screen Y position} {confidence value}` |
| `armup`        |`{camera number}/{detected person}/{left / right} {True or False}` |
| `hubs`         |`{Body Part} {normalized screen X position} {normalized screen Y position}` |
| `numberID`     |`{camera number}/{number of detected skeletons}` |
| `orientation`  |`{camera number}/{orientation of the ith body}` |
| `triangulate`  |`{detected person}/{world X position} {world Y position} {world Z position}` |
| `position`     |`{camera number}/{detected person}/{world X position} {world Y position} {world Z position}` |

### Example of Configuration
Here is an example with the default filter:
```bash
"filters": {
    "skeletons": {}
}
```

Below is an example showing how to use more than one filters and how to provide
parameters to the filters when necessary. The `"position"` filter requires
arguments that are passed through a dictionary.

```bash
"filters": {
    "armup": {},
    "position": {
        "Kmats": {
            "/dev/video0": [
            532.6468964861642,
            0.0,
            322.8857310905135,
            0.0,
            497.14279954685696,
            243.38997065789061,
            0.0,
            0.0,
            1.0
            ]
        },
        "Rtmats":{
            "/dev/video0": [
            0.4513344543194968,
            -0.8861125765727516,
            -0.10536466193046157,
            -0.14965597754962875,
            -0.411626908562561,
            -0.10197357425932596,
            -0.9056294376288826,
            -0.38168482088186123,
            0.7917452232197868,
            0.45211269811008037,
            -0.41077196802638927,
            5.957351190828515
            ]
        }
    },
}
```

### Skeletons filter
The `skeletons` filter is the default filter. It outputs the full skeleton for
each person detected by each camera, according to the
[BODY_25](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/02_output.md)
dataset.

The output is structured as follows:
```bash
{camera number}/{detected person}/{Body Part} {screen X position} {screen Y position} {confidence value}
```

Note that the various backends do not all detect as many keypoints.
All results are adjusted to match the OpenPose format for numbering keypoints.

### Armup filter
The armup filter outputs whether a person has one or both arms up.
An arm is considered up if either of the elbow or wrist keypoints are higher
than the nose or neck.

The output is structured as follows:
```bash
{camera number}/{detected person}/{left} {True or False}
{camera number}/{detected person}/{right} {True or False}
```

### Hubs filter
Filter which controls Mozilla Hubs avatar via WebSocket.

Please check the Mozilla Hubs [Documentation](https://hubs.mozilla.com/docs/welcome.html)
for more information and our [Mozilla Hubs WebSocket controller](https://gitlab.com/sat-mtl/tools/forks/hubs/-/blob/tools/src/systems/userinput/devices/websocket-controller.js) implementation to use with the `hubs` filter.
### Number of IDs filter
The "numberID" filter outputs the number of skeletons detected by camera

The output is structured as follows:
```bash
{camera number}/{number of detected skeletons}
```

### Orientation filter
Filter which outputs the orientation of detected bodies. The filter outputs an
angle between 0 and 90 degrees.
* 0 degree is perfectly facing the camera or being perfectly turned back to the camera
* 90 degree is when you are perpendicular to the camera

The output is structured as follows:
```bash
{camera number}/{orientation of the ith body}
```

Note that the orientation filter uses hardcoded values for the orientation computations.
These values need to be change according to the detected limbs lengths.

### Triangulate filter
Filter which outputs the 3D coordinates from two frames with different point of view.
Current implementation supports two cameras only and a single user detected in both frame.
The triangulation filter requires calibrated cameras.
It needs the camera matrix and the extrinsic parameters of each cameras to
compute 3D coordinates. To account for camera distortion, it is necessary to
provide the camera resolution and the distortion coefficients.

The camera matrix is passed as `"Kmats"`, the extrinsics are passed as
`"Rtmats"`, `"res"` is the camera resolution, and `"dist_coeffs"` are the
distortion coefficients:

```bash
"triangulate": {
    "Kmats": {
        "/dev/video0": [
        2666.6666,
        0.0,
        960.0,
        0.0,
        2666.6666,
        540.0,
        0.0,
        0.0,
        1.0
        ],
        "/dev/video2": [
        2666.6666,
        0.0,
        960.0,
        0.0,
        2666.6666,
        540.0,
        0.0,
        0.0,
        1.0
        ]
    },
    "Rtmats":{
        "/dev/video0": [
        0.9999771625721052,
        -0.0006078080626206253,
        0.006730891738892176,
        -0.001754582791521914,
        0.006711188507522985,
        -0.028059260457774498,
        -0.9995837322862848,
        0.0708701061760472,
        0.0007964188961626789,
        0.9996060766481574,
        -0.02805454054195237,
        0.018382768059711885
        ],
        "/dev/video2": [
        0.7671069491650818,
        0.641518913210025,
        -0.0006422900197060044,
        -3.0618360356558845,
        0.0013534269413253974,
        -0.0026195805064344047,
        -0.9999956530072943,
        0.011023618045880045,
        -0.6415178070624037,
        0.7671027452641525,
        -0.0028777473908914303,
        2.578799877302874
        ]
    },
    "dist_coeffs":{
        "/dev/video0": [
        0.1,
        0.0,
        0.0,
        0.0,
        0.0
        ],
        "/dev/video2": [
        0.07,
        0.0,
        0.0,
        0.0,
        0.0
        ]
    },
    "res": [1920, 1080]
}
```

The output is structured as follows:
```bash
{detected person}/{world X position} {world Y position} {world Z position}
```

### Position filter
The position filter, just like the triangulation filter, outputs 3D coordinates
from 2D coordinates. Specifically, it outputs the position on the horizontal
plane for each person's detected feet or ankles.

The output is structured as follows:
```bash
{camera number}/{detected person}/{world X position} {world Y position} {world Z position}
```
The position filter requires a calibrated camera.
LivePose supports two solutions:
* performing calibration for any camera supported by OpenCV
* retrieving calibration settings from Intel RealSense cameras using the `pyrealsense2` library
#### Using any camera supported by OpenCV

The position filter needs the camera matrix and the extrinsics parameters to compute the
projection on the horizontal plane.
The camera matrix is passed as `"Kmats"` and the extrinsics are passed as
`"Rtmats"`:

```bash
"position": {
    "Kmats": {
        "/dev/video0": [
        532.6468964861642,
        0.0,
        322.8857310905135,
        0.0,
        497.14279954685696,
        243.38997065789061,
        0.0,
        0.0,
        1.0
        ]
    },
    "Rtmats":{
        "/dev/video0": [
        0.4513344543194968,
        -0.8861125765727516,
        -0.10536466193046157,
        -0.14965597754962875,
        -0.411626908562561,
        -0.10197357425932596,
        -0.9056294376288826,
        -0.38168482088186123,
        0.7917452232197868,
        0.45211269811008037,
        -0.41077196802638927,
        5.957351190828515
        ]
    }
},
```
#### Using Intel RealSense cameras with the pyrealsense2 library

With a configuration including a [`"camera"`](#cameras) with `"pyrealsense2"` set as `"api"` and `"align_frames"` set to `true`; and a position filter with parameter `use_pyrealsense2` set to `true`, LivePose will retrieve calibration parameters saved in the onboard memory of Intel RealSense cameras, and align depth and color frames to perform 3D position estimation.
Passing camera matrix (`"Kmats"`) and extrinsics (`"Rtmats"`) as parameters to the position filter is thus not needed.

```bash
"position": {
    "use_pyrealsense2": true
},
```

## Outputs
LivePose offers two different standards for outputting pose estimation results:
OSC (Open Sound Control) and WebSocket.

The config file may contain an `outputs` property, which specifies the output
formats to be used and their parameters. `outputs` is a dictionary where each
key is the name of an output format, and the corresponding value is a
dictionary of parameters.

### OSC
By default, LivePose outputs its results through Open Sound Control (OSC) messages.
These can be read by a separate OSC client. We use the `pyliblo3` library for
sending and receiving OSC messages; please see the pyliblo3
[repository](https://github.com/gesellkammer/pyliblo3) for more info.

#### OSC Destinations
The `osc` section may contain a `destinations` property, which specifies all
of the addresses the results will be sent to. `destinations` is a dictionary,
where each key is an IP address (written as a string), and the corresponding
value is the port (an int).

For example:
```
"outputs": {
  "osc": {
      "destinations": {
        "127.0.0.1": 9000,
        "192.0.2.1": 3000
      }
  }
}
```

### WebSocket
LivePose also allows output in the form of WebSocket messages. This is useful for
browser-based programs that want to make use of LivePose, such as Mozilla Hubs.
We use the
[websockets](https://websockets.readthedocs.io/en/stable/) library for sending
out all WebSocket messages.

**Note**: only a single WebSocket host may be specified.

#### Websocket Host
The `websocket` section may contain a single `host` and `port` attribute
pair, specifying the address which the results will be sent to.

`host` is an IP address, written as a string. `port` is an `int`.

Example:
```
"outputs": {
  "websocket": {
    "host": "127.0.0.1",
    "port": 8765
  }
}
