#!/usr/bin/env python3

# This small script estimates the parameters of the camera
# using a checkerboard pattern and saves it to file

import argparse
import cv2
import json
import logging
import math
import numpy as np

from time import time
from typing import List, Optional, Tuple

from livepose.camera import Camera

logger = logging.getLogger(__name__)

DEFAULT_CAMERA_PATH: str = "/dev/video0"


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(formatter_class=argparse.MetavarTypeHelpFormatter)
    parser.add_argument("-c", "--camera", type=str, default=DEFAULT_CAMERA_PATH, help="Path to the camera")
    parser.add_argument("-s", "--square_size", type=float, default=21.0,
                        help="Size of the individual chessboard square in millimeters")
    parser.add_argument("-p", "--pattern_size", type=int, nargs=2, default=[
                        10, 7], help="Chessboard's number of rows and column. For a chessboard with 11 rows and 4 columns, input values like this: 11 4")
    parser.add_argument("-n", "--shot_number", type=int, default=32, help="Number of frames to capture for calibration")
    parser.add_argument("-f", "--flip", action="store_true",
                        help="Flip images around the y-axis (i.e. mirrored horizontally). Ideal for use with webcams")
    return parser.parse_args()


class Calibration:
    """
    Calibration class which takes care of camera calibration and outputs the results to a file.
    Currently handles one camera at a time
    """

    def __init__(self, cam_path: str, flip: bool, shot_number: int):
        self._flip: bool = flip
        self._camera_path: str = cam_path
        self._camera: Optional[Camera] = None
        self._calibration_time_between_shots = 0.25  # seconds between shots minimum
        self._calibration_shots_number = shot_number  # shots used for calibration
        self._calibration_last_shot: float = time()
        self._calibrated: bool = False
        self._proj_error: float = math.inf
        self._calibration_obj_points: List[np.array] = []
        self._calibration_img_points: List[np.array] = []
        self._last_update: Optional[float] = None

    @property
    def calibrated(self) -> bool:
        return self._calibrated

    def initialize_camera(self) -> bool:
        """
        Initializes the camera
        """
        self._camera = Camera(path=self._camera_path)
        if not self._camera:
            return False

        return self._camera.open()

    def run(self, pattern_size: Tuple[int, int], square_size: float) -> None:
        # size and nb of square-tiled of grid
        pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
        pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)
        pattern_points *= square_size

        if not self._camera or not self._camera.grab() or \
                not self._camera.retrieve():
            return

        assert(self._camera.frame is not None)
        bw_frame = cv2.cvtColor(self._camera.frame, cv2.COLOR_BGR2GRAY)
        found, corners = cv2.findChessboardCorners(bw_frame, pattern_size)

        if found:
            cv2.cornerSubPix(bw_frame, corners, (5, 5), (-1, -1),
                             (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1))
            cv2.drawChessboardCorners(self._camera.frame, pattern_size, corners, found)

            text_frame = np.zeros(self._camera.frame.shape, dtype=self._camera.frame.dtype)
            cv2.putText(text_frame,
                        "Calibration completion: {}%".format(
                            round(100.0 * len(self._calibration_obj_points) / self._calibration_shots_number)),
                        (8, 24),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1,
                        (255, 255, 255))

            if self._flip:
                text_frame = np.flip(text_frame, axis=1)

            # add the text to the frame
            self._camera._frame = cv2.add(self._camera.frame, text_frame)

            current_time = time()
            if current_time - self._calibration_last_shot < self._calibration_time_between_shots:
                return

            self._calibration_last_shot = current_time

            self._calibration_obj_points.append(pattern_points)
            self._calibration_img_points.append(corners)

            if len(self._calibration_obj_points) < self._calibration_shots_number:
                return

            proj_error, camera_matrix, dist_coeffs, Rvec, tvec = cv2.calibrateCamera(
                objectPoints=self._calibration_obj_points,
                imagePoints=self._calibration_img_points,
                imageSize=(self._camera.frame.shape[1], self._camera.frame.shape[0]),
                cameraMatrix=self._camera.kmat,
                distCoeffs=self._camera.dist_coeffs,
                flags=cv2.CALIB_ZERO_TANGENT_DIST
            )
            self._proj_error = proj_error
            self._camera.kmat = camera_matrix
            self._camera.dist_coeffs = dist_coeffs.reshape((5, -1))
            (rotation_mat, jacobian) = cv2.Rodrigues(Rvec[0])
            self._camera.extrinsics = np.c_[rotation_mat, tvec[0]]
            self._calibrated = True

    def display_camera(self) -> None:
        if not self._camera:
            return

        frame = self._camera.frame
        if frame is None:
            return

        # Flip the image horizontally to act as a mirror view
        if self._flip:
            frame = np.flip(frame, axis=1)
        cv2.imshow("Calibration Process", frame)
        cv2.waitKey(1)

    def save2file(self) -> None:
        if not self._camera:
            return

        if self._proj_error < 10.0:
            assert(self._camera.frame is not None)
            assert(self._camera.kmat is not None)
            assert(self._camera.dist_coeffs is not None)
            assert(self._camera.extrinsics is not None)

            # save the results to file
            cam_data = {
                "proj_error": self._proj_error,
                "camera_matrix": self._camera.kmat.flatten().tolist(),
                "distortion_coeffs": self._camera.dist_coeffs.flatten().tolist(),
                # "extrinsics": self._camera.extrinsics.flatten().tolist(),
                # we want to be compatible with OpenCV matrix notations
                # frame_resolution is thus saved as [src.cols, src.rows]
                "frame_resolution": [self._camera.frame.shape[1], self._camera.frame.shape[0]]
            }

            with open("camera_parameters.json", "w", encoding="utf-8") as f:
                json.dump(cam_data, f, ensure_ascii=False, indent=4)
            logger.info("Calibration parameters saved to file: camera_parameters.json")

        else:
            logger.error("Calibration not saved because the reprojection error was too high:", self._proj_error)


if __name__ == "__main__":
    args = parse_args()
    CAMERA_PATH = args.camera
    FLIP = args.flip
    # The algorithm cv2.findChessboardCorners expects the pattern size to be in terms of inner corners
    PATTERN_SIZE = (args.pattern_size[0] - 1, args.pattern_size[1] - 1)
    SQUARE_SIZE = args.square_size
    SHOT_NUMBER = args.shot_number

    calib_obj = Calibration(cam_path=CAMERA_PATH, flip=FLIP, shot_number=SHOT_NUMBER)
    if not calib_obj.initialize_camera():
        logger.error("Camera could not be initialized")

    while not calib_obj.calibrated:
        calib_obj.run(pattern_size=PATTERN_SIZE, square_size=SQUARE_SIZE)
        calib_obj.display_camera()

    logger.info("Calibration completed")
    calib_obj.save2file()
