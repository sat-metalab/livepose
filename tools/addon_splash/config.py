import json
import logging
import numpy as np
import os

from typing import Any, Dict, List

logger = logging.getLogger(__name__)


class Config:
    """
    Utility class taking care of loading the config file and holding the settings
    """

    def __init__(self, path: str) -> None:
        if not os.path.exists(path):
            logger.error(f"Error on {path}: {path} does not exists or permission is not granted to execute")
        self._path: str = path

        with open(self._path) as f:
            try:
                data = json.load(f)
            except Exception:
                logger.error(f"Error in configuration file: {path}, file could not be loaded")

        # make sure that all the config parameters are presents
        self._planar: bool = data["planar"]
        for param in ["planar", "calib_pts", "input.paths"]:
            if param not in data:
                logger.error(f"Configuration file: {self._path} is missing parameter \"{param}\"")

        if len(data["calib_pts"]) < 6:
            logger.error("Error in the configuration file, a minimum of 6 points are required for calibration")

        self._calib_pts: List[List[float]] = data["calib_pts"]
        cam_paths: Dict[str, str] = data["input.paths"]

        # intrinsic parameters are optional in the case of planar calibration
        try:
            kmat_paths: Dict[str, str] = data["input.intrinsics_paths"]
        except Exception:
            if self._planar:
                kmat_paths = {}
            else:
                logger.error("For non-plannar surfaces, intrinsic parameters must be supplied in the configuration file")
                raise ValueError(
                    "For non-plannar surfaces, intrinsic parameters must be supplied in the configuration file")

        # load the intrinsic parameters if provided
        # by looping over the cameras and initialize some attributes
        # we contruct a dictionary like this:
        # {
        #   "cam_nickname": {
        #       "path": path/to/cam
        #       "camera_matrix": np.array of intrinsic parameters,
        #       "dist_coeff" : np.array of distortion coeff
        #    }
        #   ....
        # }
        self._cams_info: Dict[str, Any] = {}
        for name, path in cam_paths.items():
            self._cams_info[name] = {"path": path}
            if kmat_paths and kmat_paths[name]:
                if not os.path.exists(kmat_paths[name]):
                    logger.error(
                        f"Error on {kmat_paths[name]}: {kmat_paths[name]} does not exists or permission is not granted to execute")
                with open(kmat_paths[name]) as f:
                    data = json.load(f)
                    self._cams_info[name]["camera_matrix"] = np.array([data["camera_matrix"]]).reshape((3, 3))
                    self._cams_info[name]["dist_coeffs"] = np.array([data["distortion_coeffs"]]).reshape((5, 1))

    @property
    def calib_pts(self) -> List[List[float]]:
        return self._calib_pts

    @property
    def planar(self) -> bool:
        return self._planar

    @property
    def cams_info(self) -> Dict[str, Any]:
        return self._cams_info
